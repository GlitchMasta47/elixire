from setuptools import setup

setup(
    name='elixire',
    version='2',
    description='Image host',
    url='https://elixi.re',
    author='Ave Ozkal, Luna Mendes, Mary Strodl',
    python_requires='>=3.6'
)
